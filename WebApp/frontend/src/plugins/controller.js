const Controller = {
  install(Vue, options) {
    Vue.mixin({
      mounted() {
        
      },
      methods: {
        logout: function (event) {
          this.$cookie.set('token', null, { expires: '1M' });
          this.$router.push({ name: "home" });
        },
        goToDashboard() {
          this.$router.push({ name: "dashboard" });
        }
      }
    });
  }
};

export default Controller;
