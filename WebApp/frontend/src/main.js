import Vue from 'vue'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import Controller from "./plugins/controller"
import * as VueGoogleMaps from 'vue2-google-maps'


Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAnJ4Kb_6Mm9YlV7eDRwOtMKmtLficLMZY',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
  }
});

const VueCookie = require('vue-cookie');

require('./css/base.css');

Vue.config.productionTip = false;

const moment = require('moment');
require('moment/locale/pt');
Vue.use(require('vue-moment'), { moment });

Vue.use(VueAxios, axios);
Vue.use(Controller);
Vue.use(VueCookie);
Vue.use(VueRouter);

import Home from "./components/public/Home"
import Login from "./components/public/Login"
import CreaterUser from "./components/public/CreateUser"
import CreaterUserDone from "./components/public/CreateUserDone"
import EmailValidation from "./components/public/EmailValidation"

import Dashboard from "./components/private/Dashboard"
import CreateEvent from "./components/private/event/Create"
import ViewEvent from "./components/private/event/View"
import AdminEvent from "./components/private/event/Admin"

const routes = [
  //public routes
  { path: '/', component: Home, name: "home" },
  { path: '/login', component: Login, name: "login" },
  { path: '/emailvalidation/:status', component: EmailValidation, name: "emailValidation" },
  { path: '/createuser/done', component: CreaterUserDone, name: "createUserDone" },
  { path: '/createuser/:email?/:user?', component: CreaterUser, name: "createUser" },

  //private rotes
  { path: '/dashboard', component: Dashboard, name: "dashboard" },
  { path: '/event/view/:id', component: ViewEvent, name: "viewEvent" },
  { path: '/event/create', component: CreateEvent, name: "createEvent" },
  { path: '/event/admin/:id', component: AdminEvent, name: "admin" }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
