﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace WebApp
{
    public static class Utilities
    {
        public enum Roles { Administrator, Contributor, SellerAndValidator, Seller, Validator, Observer };

        public static String Sha256(String randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
        public static int GetUserId(HttpContext ctx)
        {
            Int32.TryParse(ctx.Request.Headers.SingleOrDefault(buffer => buffer.Key == "UserId").Value.ToString(), out int result);
            return result;
        }
    }
}
