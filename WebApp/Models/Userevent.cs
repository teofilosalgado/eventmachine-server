﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class Userevent
    {
        public int Iduserevent { get; set; }
        public int Iduser { get; set; }
        public int Idevent { get; set; }
        public int Userrole { get; set; }

        public Event IdeventNavigation { get; set; }
        public User IduserNavigation { get; set; }
    }
}
