﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class Ticketbatch
    {
        public Ticketbatch()
        {
            Ticket = new HashSet<Ticket>();
        }

        public int Id { get; set; }
        public int Idevent { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Totalquantity { get; set; }
        public int Available { get; set; }
        public bool Isactive { get; set; }
        public DateTimeOffset? Salesstartdate { get; set; }
        public DateTimeOffset? Salesenddate { get; set; }
        public string Ticketlogo { get; set; }

        public Event IdeventNavigation { get; set; }
        public ICollection<Ticket> Ticket { get; set; }
    }
}
