﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Models
{
    public partial class DatabaseContext : DbContext
    {
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Finance> Finance { get; set; }
        public virtual DbSet<Ticket> Ticket { get; set; }
        public virtual DbSet<Ticketbatch> Ticketbatch { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Userevent> Userevent { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql(@"Host=babar.elephantsql.com;Database=rayhxcbz;Username=rayhxcbz;Password=3brVgma3jFi_j7e6q2dieVt-VWjMBlKU");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("btree_gin")
                .HasPostgresExtension("btree_gist")
                .HasPostgresExtension("citext")
                .HasPostgresExtension("cube")
                .HasPostgresExtension("dblink")
                .HasPostgresExtension("dict_int")
                .HasPostgresExtension("dict_xsyn")
                .HasPostgresExtension("earthdistance")
                .HasPostgresExtension("fuzzystrmatch")
                .HasPostgresExtension("hstore")
                .HasPostgresExtension("intarray")
                .HasPostgresExtension("ltree")
                .HasPostgresExtension("pg_stat_statements")
                .HasPostgresExtension("pg_trgm")
                .HasPostgresExtension("pgcrypto")
                .HasPostgresExtension("pgrowlocks")
                .HasPostgresExtension("pgstattuple")
                .HasPostgresExtension("plv8")
                .HasPostgresExtension("tablefunc")
                .HasPostgresExtension("unaccent")
                .HasPostgresExtension("uuid-ossp")
                .HasPostgresExtension("xml2");

            modelBuilder.Entity<Event>(entity =>
            {
                entity.ToTable("event");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Enddate).HasColumnName("enddate");

                entity.Property(e => e.Eventcover).HasColumnName("eventcover");

                entity.Property(e => e.Eventpicture).HasColumnName("eventpicture");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Location).HasColumnName("location");

                entity.Property(e => e.Locationname)
                    .IsRequired()
                    .HasColumnName("locationname");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Startdate).HasColumnName("startdate");
            });

            modelBuilder.Entity<Finance>(entity =>
            {
                entity.ToTable("finance");

                entity.HasIndex(e => e.Idevent)
                    .HasName("finance_idevent_idx");

                entity.HasIndex(e => e.Iduser)
                    .HasName("finance_iduser_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description");

                entity.Property(e => e.Idevent).HasColumnName("idevent");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Isprotected)
                    .HasColumnName("isprotected")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Sign).HasColumnName("sign");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title");

                entity.Property(e => e.Value)
                    .HasColumnName("value")
                    .HasColumnType("numeric(10, 2)");

                entity.HasOne(d => d.IdeventNavigation)
                    .WithMany(p => p.Finance)
                    .HasForeignKey(d => d.Idevent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_finance__idevent");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Finance)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_finance__iduser");
            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.ToTable("ticket");

                entity.HasIndex(e => e.Idticketbatch)
                    .HasName("ticket_idticketbatch_idx");

                entity.HasIndex(e => e.Iduser)
                    .HasName("ticket_iduser_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Buyername)
                    .IsRequired()
                    .HasColumnName("buyername");

                entity.Property(e => e.Buyerphone).HasColumnName("buyerphone");

                entity.Property(e => e.Idticketbatch).HasColumnName("idticketbatch");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Purchasedate).HasColumnName("purchasedate");

                entity.Property(e => e.Tickethash)
                    .IsRequired()
                    .HasColumnName("tickethash");

                entity.HasOne(d => d.IdticketbatchNavigation)
                    .WithMany(p => p.Ticket)
                    .HasForeignKey(d => d.Idticketbatch)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ticket__idticketbatch");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Ticket)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ticket__iduser");
            });

            modelBuilder.Entity<Ticketbatch>(entity =>
            {
                entity.ToTable("ticketbatch");

                entity.HasIndex(e => e.Idevent)
                    .HasName("ticketbatch_idevent_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Available).HasColumnName("available");

                entity.Property(e => e.Idevent).HasColumnName("idevent");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("numeric(10, 2)");

                entity.Property(e => e.Salesenddate).HasColumnName("salesenddate");

                entity.Property(e => e.Salesstartdate).HasColumnName("salesstartdate");

                entity.Property(e => e.Ticketlogo).HasColumnName("ticketlogo");

                entity.Property(e => e.Totalquantity).HasColumnName("totalquantity");

                entity.HasOne(d => d.IdeventNavigation)
                    .WithMany(p => p.Ticketbatch)
                    .HasForeignKey(d => d.Idevent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ticketbatch__idevent");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.HasIndex(e => new { e.Email, e.Nick, e.Notificationtoken })
                    .HasName("user_email_nick_notificationtoken_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email");

                entity.Property(e => e.Isvalid).HasColumnName("isvalid");

                entity.Property(e => e.Joindate).HasColumnName("joindate");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.Nick)
                    .IsRequired()
                    .HasColumnName("nick");

                entity.Property(e => e.Notificationtoken).HasColumnName("notificationtoken");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone");

                entity.Property(e => e.Profilecover).HasColumnName("profilecover");

                entity.Property(e => e.Profilepicture).HasColumnName("profilepicture");
            });

            modelBuilder.Entity<Userevent>(entity =>
            {
                entity.HasKey(e => e.Iduserevent);

                entity.ToTable("userevent");

                entity.HasIndex(e => e.Idevent)
                    .HasName("userevent_idevent_idx");

                entity.HasIndex(e => e.Iduser)
                    .HasName("userevent_iduser_idx");

                entity.Property(e => e.Iduserevent).HasColumnName("iduserevent");

                entity.Property(e => e.Idevent).HasColumnName("idevent");

                entity.Property(e => e.Iduser).HasColumnName("iduser");

                entity.Property(e => e.Userrole).HasColumnName("userrole");

                entity.HasOne(d => d.IdeventNavigation)
                    .WithMany(p => p.Userevent)
                    .HasForeignKey(d => d.Idevent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userevent__idevent");

                entity.HasOne(d => d.IduserNavigation)
                    .WithMany(p => p.Userevent)
                    .HasForeignKey(d => d.Iduser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_userevent__iduser");
            });
        }
    }
}
