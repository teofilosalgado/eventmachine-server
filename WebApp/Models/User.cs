﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class User
    {
        public User()
        {
            Finance = new HashSet<Finance>();
            Ticket = new HashSet<Ticket>();
            Userevent = new HashSet<Userevent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTimeOffset Joindate { get; set; }
        public bool Isvalid { get; set; }
        public string Nick { get; set; }
        public string Phone { get; set; }
        public string Profilepicture { get; set; }
        public string Profilecover { get; set; }
        public string Notificationtoken { get; set; }

        public ICollection<Finance> Finance { get; set; }
        public ICollection<Ticket> Ticket { get; set; }
        public ICollection<Userevent> Userevent { get; set; }
    }
}
