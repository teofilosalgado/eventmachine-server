﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public int Idticketbatch { get; set; }
        public int Iduser { get; set; }
        public DateTimeOffset Purchasedate { get; set; }
        public string Tickethash { get; set; }
        public string Buyername { get; set; }
        public string Buyerphone { get; set; }

        public Ticketbatch IdticketbatchNavigation { get; set; }
        public User IduserNavigation { get; set; }
    }
}
