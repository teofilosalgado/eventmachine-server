﻿using System;
using System.Collections.Generic;

namespace WebApp.Models
{
    public partial class Finance
    {
        public int Id { get; set; }
        public int Idevent { get; set; }
        public int Iduser { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Value { get; set; }
        public bool Sign { get; set; }
        public bool? Isprotected { get; set; }
        public int Quantity { get; set; }

        public Event IdeventNavigation { get; set; }
        public User IduserNavigation { get; set; }
    }
}
