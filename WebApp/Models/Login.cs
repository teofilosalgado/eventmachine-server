﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class Login
    {
        public String Email { get; set; }
        public String Password { get; set; }
    }
}
