﻿using System;
using System.Collections.Generic;
using NpgsqlTypes;

namespace WebApp.Models
{
    public partial class Event
    {
        public Event()
        {
            Finance = new HashSet<Finance>();
            Ticketbatch = new HashSet<Ticketbatch>();
            Userevent = new HashSet<Userevent>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Startdate { get; set; }
        public DateTimeOffset? Enddate { get; set; }
        public string Eventpicture { get; set; }
        public string Eventcover { get; set; }
        public NpgsqlPoint Location { get; set; }
        public string Locationname { get; set; }
        public bool Isactive { get; set; }

        public ICollection<Finance> Finance { get; set; }
        public ICollection<Ticketbatch> Ticketbatch { get; set; }
        public ICollection<Userevent> Userevent { get; set; }
    }
}
