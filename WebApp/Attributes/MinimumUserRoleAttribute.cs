﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp.Attributes
{
    public class MinimumUserRoleAttribute : ActionFilterAttribute
    {
        private int _role;

        public MinimumUserRoleAttribute(Utilities.Roles arg)
        {
            _role = (int)arg;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            try
            {
                String reqMethod = (filterContext.HttpContext.Request.Method);
                if(reqMethod == "GET")
                {
                    int eventid = (int)filterContext.ActionArguments["eventId"];
                    using (DatabaseContext db = new DatabaseContext())
                    {
                        Userevent confirmRole = db.Userevent.SingleOrDefault(buffer => (buffer.Iduser == Utilities.GetUserId(filterContext.HttpContext)) && (buffer.Idevent == eventid) && (buffer.Userrole <= _role));
                        if ((confirmRole == null))
                        {
                            Controller controller = filterContext.Controller as Controller;
                            filterContext.Result = controller.Unauthorized();
                        }
                    }
                }
                else
                {
                    JObject model = filterContext.ActionArguments["data"] as JObject;
                    int eventid = (int)model["EventId"];

                    String password = Utilities.Sha256((String)model["CurrentPassword"]);
                    using (DatabaseContext db = new DatabaseContext())
                    {
                        User confirmPassword = db.User.SingleOrDefault(buffer => (buffer.Id == Utilities.GetUserId(filterContext.HttpContext)) && (buffer.Password == password));
                        Userevent confirmRole = db.Userevent.SingleOrDefault(buffer => (buffer.Iduser == Utilities.GetUserId(filterContext.HttpContext)) && (buffer.Idevent == eventid) && (buffer.Userrole <= _role));
                        if ((confirmRole == null) || (confirmPassword == null))
                        {
                            Controller controller = filterContext.Controller as Controller;
                            filterContext.Result = controller.Unauthorized();
                        }
                    }
                }
            }
            catch
            {
                Controller controller = filterContext.Controller as Controller;
                filterContext.Result = controller.BadRequest();
            }
        }
    }
}
