﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApp.Models;

namespace WebApp
{
    public partial class Startup
    {
        public SymmetricSecurityKey key;
        public int UserId;

        private void ConfigureAuth(IServiceCollection services)
        {
            key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("TokenAuthentication:SecretKey").Value));
            TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,
                ValidateIssuer = false,
                AudienceValidator = (audiences, token, parameters) =>
                {
                    bool result = Int32.TryParse(token.Issuer, out int id);
                    if (result == true)
                    {
                        using (DatabaseContext db = new DatabaseContext())
                        {
                            User query = db.User.SingleOrDefault(buffer => buffer.Id == id);
                            if (query == null)
                            {
                                return false;
                            }
                            else
                            {
                                UserId = query.Id;
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }
                },
                ValidateAudience = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
            };
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        context.HttpContext.Request.Headers.Add("UserId", UserId.ToString());
                        return Task.CompletedTask;
                    }
                };
                options.TokenValidationParameters = tokenValidationParameters;
            });

            services.AddIdentity<IdentityUser, IdentityRole>();
            services.AddAuthentication(v =>
            {
                v.DefaultAuthenticateScheme = GoogleDefaults.AuthenticationScheme;
                v.DefaultChallengeScheme = GoogleDefaults.AuthenticationScheme;
            }).AddGoogle(googleOptions =>
            {
                googleOptions.ClientId = "192159289271-kvbs27p1q5b79jrqa5paepmp08e0m15m.apps.googleusercontent.com ";
                googleOptions.ClientSecret = "3piOI1b0Rd0w-IAJ0MbLPi6W";
                googleOptions.Scope.Add("profile");
            });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme).RequireAuthenticatedUser().Build();
            });
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            });
        }
    }
}
