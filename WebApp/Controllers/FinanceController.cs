﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApp.Attributes;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Finance")]
    public class FinanceController : Controller
    {
        [HttpGet]
        [Authorize]
        [Route("{eventId:int}/{entryId:int}")]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult ViewEntry(int eventId, int entryId)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Finance toSend = db.Finance.SingleOrDefault(buffer => (buffer.Id == entryId));
                    return Ok(Json(toSend).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [Route("{eventId:int}")]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult GetAllEntries(int eventId)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Finance> query = db.Finance.Where(buffer => (buffer.Idevent == eventId));
                    JArray toSend = new JArray();
                    foreach (Finance entry in query)
                    {
                        JObject buffer = JObject.FromObject(entry);
                        buffer.Remove("IdeventNavigation");
                        buffer.Remove("IduserNavigation");
                        toSend.Add(buffer);
                    }
                    return Ok(Json(toSend).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult UpdateEntry([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    int id = (int)data["EntryId"];
                    Finance result = db.Finance.SingleOrDefault(buffer => (buffer.Id == id));

                    JObject entry = JObject.Parse(data["Entry"].ToString());
                    Finance body = entry.ToObject<Finance>();
                    IList<String> keys = entry.Properties().Select(p => p.Name).ToList();
                    foreach (String key in keys)
                    {
                        PropertyInfo pinfo = typeof(Finance).GetProperty(key);
                        object value = pinfo.GetValue(body, null);
                        pinfo.SetValue(result, value);
                    }

                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Administrator)]
        public IActionResult DeleteEntry([FromBody] JObject data)
        {
            using (DatabaseContext db = new DatabaseContext())
            {
                Finance toRemove = db.Finance.SingleOrDefault(buffer => buffer.Id == (int)data["EntryId"]);
                if(toRemove.Isprotected == true)
                {
                    return Unauthorized();
                }
                db.Finance.Remove(toRemove);
                db.SaveChanges();
                return Ok();
            }
        }

        [HttpPost]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult CreateEntry([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Finance result = data.ToObject<Finance>();
                    result.Idevent = (int)data["EventId"];
                    result.Iduser = Utilities.GetUserId(HttpContext);
                    result.Date = DateTime.Now;
                    db.Finance.Add(result);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}