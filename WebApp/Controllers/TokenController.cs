﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Route("api/Token")]
    public class TokenController : Controller
    {
        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody]Login login)
        {
            var user = Authenticate(login);
            if (user != null)
            {
                String tokenString = BuildToken(user);
                if (tokenString == null)
                {
                    return Unauthorized();
                }
                else
                {
                    return Ok(new { token = tokenString });
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        private IConfiguration Configuration;
        public TokenController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private string BuildToken(User user)
        {
            if (user.Isvalid == true)
            {
                Claim[] claims = new[] {
                    new Claim("iss", user.Id.ToString())
                };
                SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["TokenAuthentication:SecretKey"]));
                SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                JwtSecurityToken token = new JwtSecurityToken(null, null, claims, expires: DateTime.Now.AddMonths(1), signingCredentials: creds);
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            else
            {
                return null;
            }
        }

        private User Authenticate(Login login)
        {
            login.Password = Utilities.Sha256(login.Password);
            using (DatabaseContext ctx = new DatabaseContext())
            {
                IQueryable<User> user = from buffer in ctx.User where ((buffer.Email == login.Email) && (buffer.Password == login.Password)) select buffer;
                return user.FirstOrDefault<User>();
            }
        }
    }
}