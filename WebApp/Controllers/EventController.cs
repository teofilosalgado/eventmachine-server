﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApp.Attributes;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Event")]
    public class EventController : Controller
    {
        [HttpGet]
        [Authorize]
        [Route("{eventId:int}")]
        [MinimumUserRole(Utilities.Roles.Observer)]
        public IActionResult ViewEvent(int eventId)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Event result = db.Event.SingleOrDefault(buffer => buffer.Id == eventId);
                    JObject toSend = JObject.FromObject(new
                    {
                        result.Id,
                        result.Name,
                        result.Description,
                        result.Startdate,
                        result.Enddate,
                        result.Eventpicture,
                        result.Eventcover,
                        result.Location,
                        result.Locationname,
                        result.Isactive
                    });
                    return Ok(Json(toSend).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateEvent([FromBody] Event requestEvent)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    User result = db.User.SingleOrDefault(buffer => buffer.Id == Utilities.GetUserId(HttpContext));
                    int userId = result.Id;

                    Event eventBuffer = new Event();
                    eventBuffer = requestEvent;
                    db.Event.Add(eventBuffer);
                    db.SaveChanges();
                    int eventId = eventBuffer.Id;

                    Userevent usereventBuffer = new Userevent();
                    usereventBuffer.Iduser = userId;
                    usereventBuffer.Idevent = eventId;
                    usereventBuffer.Userrole = (int)Utilities.Roles.Administrator;
                    db.Userevent.Add(usereventBuffer);
                    db.SaveChanges();
                }
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult UpdateEvent([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    int eventId = (int)data["EventId"];
                    Event result = db.Event.SingleOrDefault(buffer => (buffer.Id == eventId));

                    JObject entry = JObject.Parse(data["Event"].ToString());
                    Event body = entry.ToObject<Event>();
                    IList<String> keys = entry.Properties().Select(p => p.Name).ToList();
                    foreach (String key in keys)
                    {
                        PropertyInfo pinfo = typeof(Event).GetProperty(key);
                        object value = pinfo.GetValue(body, null);
                        pinfo.SetValue(result, value);
                    }

                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }

        }

        [HttpDelete]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Administrator)]
        public IActionResult DeleteEvent([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    int eventId = (int)data["EventId"];
                    Event eventToRemove = db.Event.SingleOrDefault(buffer => buffer.Id == eventId);

                    IQueryable<Ticketbatch> list = db.Ticketbatch.Where(buffer => (buffer.Idevent == eventId));
                    List<int> idList = new List<int>();
                    foreach (Ticketbatch tb in list)
                    {
                        idList.Add(tb.Id);
                    }
                    idList = idList.Distinct().ToList();
                    foreach (int id in idList)
                    {
                        db.Ticket.RemoveRange(db.Ticket.Where(buffer => (buffer.Idticketbatch == id)));
                    }

                    db.Ticketbatch.RemoveRange(db.Ticketbatch.Where(buffer => (buffer.Idevent == eventId)));
                    db.Userevent.RemoveRange(db.Userevent.Where(buffer => (buffer.Idevent == eventId)));
                    db.Event.Remove(eventToRemove);

                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}