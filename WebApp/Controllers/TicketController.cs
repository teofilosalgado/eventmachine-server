﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using WebApp.Attributes;
using WebApp.Models;
namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Ticket")]
    public class TicketController : Controller
    {
        [Authorize]
        [HttpGet]
        [Route("SearchTicketByBuyerName/{eventId:int}/{TicketBatchId:int}/{buyerName}")]
        [MinimumUserRole(Utilities.Roles.Validator)]
        public IActionResult SearchTicketByBuyerName(int eventId, int TicketBatchId, string buyerName)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Ticket> tc = db.Ticket.Where(buffer => buffer.Buyername.Contains(buyerName));
                    JArray jArray = new JArray();
                    foreach (Ticket temp in tc)
                    {
                        JObject jObject = JObject.FromObject(temp);
                        jArray.Add(jObject);
                    }
                    return Ok((Json(jArray).Value));
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("SearchTicketByBuyerPhone/{eventId:int}/{TicketBatchId:int}/{buyerPhone}")]
        [MinimumUserRole(Utilities.Roles.Validator)]
        public IActionResult SearchTicketByBuyerPhone(int eventId, int TicketBatchId, string buyerPhone)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Ticket> tc = db.Ticket.Where(buffer => buffer.Buyerphone.Contains(buyerPhone));
                    JArray jArray = new JArray();
                    foreach (Ticket temp in tc)
                    {
                        JObject jObject = JObject.FromObject(temp);
                        jArray.Add(jObject);
                    }
                    return Ok((Json(jArray).Value));
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpPut]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult UpdateTicket([FromBody]JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    int tbId = (int)data["TicketBatchId"];
                    int ticketId = (int)data["TicketId"];
                    Ticket result = db.Ticket.SingleOrDefault(buffer => (buffer.Idticketbatch == tbId) && (buffer.Id == ticketId));

                    JObject entry = JObject.Parse(data["Ticket"].ToString());
                    Ticket body = entry.ToObject<Ticket>();
                    IList<String> keys = entry.Properties().Select(p => p.Name).ToList();
                    foreach (String key in keys)
                    {
                        PropertyInfo pinfo = typeof(Ticket).GetProperty(key);
                        object value = pinfo.GetValue(body, null);
                        pinfo.SetValue(result, value);
                    }

                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpDelete]
        [MinimumUserRole(Utilities.Roles.Administrator)]
        public IActionResult DeleteTicket([FromBody]JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Ticketbatch toUpdate = db.Ticketbatch.SingleOrDefault(buffer => (buffer.Id == (int)data["TicketBatchId"]) && (buffer.Idevent == (int)data["EventId"]));
                    Ticket toDelete = db.Ticket.SingleOrDefault(buffer => (buffer.Id == (int)data["TicketId"]) && (buffer.Idticketbatch == (int)data["TicketBatchId"]));

                    db.Ticket.Remove(toDelete);
                    toUpdate.Available = toUpdate.Available + 1;


                    Finance entry = db.Finance.SingleOrDefault(buffer => buffer.Title == ("Vendas do lote: " + toUpdate.Name + ".") && buffer.Value == toUpdate.Price);
                    entry.Quantity = entry.Quantity - 1;


                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpPost]
        [MinimumUserRole(Utilities.Roles.Seller)]
        public IActionResult SellTicket([FromBody]JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Event ev = db.Event.SingleOrDefault(buffer => buffer.Id == (int)data["EventId"]);
                    Ticketbatch tb = db.Ticketbatch.SingleOrDefault(buffer => (buffer.Id == (int)data["TicketBatchId"]));
                    if ((tb.Available > 0) && (tb.Isactive == true) && (ev.Isactive == true))
                    {
                        tb.Available = tb.Available - 1;
                    }
                    else
                    {
                        return BadRequest();
                    }

                    JObject workarea = (JObject)data["Ticket"];
                    workarea.Add("Iduser", Utilities.GetUserId(HttpContext));
                    workarea.Add("Purchasedate", DateTime.Now);
                    workarea.Add("Tickethash", Utilities.Sha256(DateTime.Now.ToString()));
                    Ticket toAdd = workarea.ToObject<Ticket>();
                    db.Ticket.Add(toAdd);

                    if(db.Finance.Where(buffer => buffer.Title == ("Vendas do lote: " + tb.Name + ".") && buffer.Value == tb.Price).Count() == 0)
                    {
                        Finance entry = new Finance();
                        entry.Date = DateTime.Now;
                        entry.Description = "Venda de ingressos, do lote: " + tb.Name + "." + " Geradas automaticamente após cada transação.";
                        entry.Idevent = ev.Id;
                        entry.Iduser = Utilities.GetUserId(HttpContext);
                        entry.Isprotected = true;
                        entry.Sign = true;
                        entry.Title = "Vendas do lote: " + tb.Name;
                        entry.Value = tb.Price;
                        db.Finance.Add(entry);
                    }
                    else
                    {
                        Finance toUpdate = db.Finance.SingleOrDefault(buffer => buffer.Title == ("Vendas do lote: " + tb.Name + "."));
                        toUpdate.Quantity = toUpdate.Quantity + 1;
                    }
                    

                    db.SaveChanges();

                    workarea.Add("Logo", tb.Ticketlogo);
                    workarea.Remove("Idticketbatch");
                    workarea.Remove("Iduser");
                    return Ok(Json(workarea).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}