﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        [HttpGet]
        [Authorize]
        [Route("SearchUserByNick/{nick}")]
        public IActionResult SearchUserByName(string nick)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable users = db.User.Where(buffer => (buffer.Nick.Contains(nick)) && (buffer.Isvalid == true)).Take(5);
                    JArray jArray = new JArray();
                    foreach (User temp in users)
                    {
                        JObject jObject = JObject.FromObject(temp);
                        jObject.Remove("Password");
                        jObject.Remove("Isvalid");
                        jObject.Remove("Email");
                        jObject.Remove("Userevent");
                        jArray.Add(jObject);
                    }
                    return Ok((Json(jArray).Value));
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [Route("SearchUserByPhone/{phone}")]
        public IActionResult SearchUserByPhone(string phone)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable users = db.User.Where(buffer => (buffer.Phone == (phone)) && (buffer.Isvalid == true));
                    JArray jArray = new JArray();
                    foreach (User temp in users)
                    {
                        JObject jObject = JObject.FromObject(temp);
                        jObject.Remove("Password");
                        jObject.Remove("Isvalid");
                        jObject.Remove("Email");
                        jObject.Remove("Userevent");
                        jArray.Add(jObject);
                    }
                    return Ok((Json(jArray).Value));
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [Route("Profile")]
        public IActionResult ViewUserProfile()
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    User result = db.User.SingleOrDefault(buffer => buffer.Id == Utilities.GetUserId(HttpContext));
                    if (result == null)
                    {
                        return BadRequest();
                    }
                    else
                    {
                        JObject buffer = JObject.FromObject(new
                        {
                            result.Name,
                            result.Email,
                            result.Joindate,
                            result.Nick,
                            result.Phone,
                            result.Profilepicture,
                            result.Profilecover
                        });
                        return Ok(Json(buffer).Value);
                    }
                }
            }
            catch
            {
                return BadRequest();
            }

        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult CreateUser([FromBody]User user)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    User userBuffer = new User();
                    userBuffer = user;
                    userBuffer.Isvalid = false;
                    userBuffer.Joindate = DateTime.Now;
                    userBuffer.Password = Utilities.Sha256(user.Password);
                    db.User.Add(userBuffer);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        public IActionResult DeleteUser([FromBody]JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Userevent> eventsThatIAdm = db.Userevent.Where(buffer => (buffer.Iduser == Utilities.GetUserId(HttpContext)) && (buffer.Userrole == (int)Utilities.Roles.Administrator));
                    foreach(Userevent ue in eventsThatIAdm)
                    {
                        IQueryable<Userevent> admCount = db.Userevent.Where(buffer => (buffer.Idevent == ue.Idevent) && (buffer.Userrole == (int)Utilities.Roles.Administrator));
                        if(admCount.Count() == 1)
                        {
                            return BadRequest();
                        }
                    }

                    IQueryable<Ticket> ticketsThatISold = db.Ticket.Where(buffer => (buffer.Iduser == Utilities.GetUserId(HttpContext)));
                    if(ticketsThatISold.Count() != 0)
                    {
                        return BadRequest();
                    }

                    IQueryable<Finance> financesEntries = db.Finance.Where(buffer => (buffer.Iduser == Utilities.GetUserId(HttpContext)));
                    if(financesEntries.Count() != 0)
                    {
                        return BadRequest();
                    }

                    User toRemove = db.User.SingleOrDefault(buffer => (buffer.Id == Utilities.GetUserId(HttpContext)) && (buffer.Password == Utilities.Sha256((String)data["Password"])));
                    if (toRemove == null)
                    {
                        return Unauthorized();
                    }
                    else
                    {
                        Userevent userevent = db.Userevent.SingleOrDefault(buffer => buffer.Iduser == toRemove.Id);
                        db.Userevent.Remove(userevent);
                        db.User.Remove(toRemove);
                        db.SaveChanges();
                        return Ok();
                    }
                }
            }
            catch
            {
                return Unauthorized();
            }
        }

        [HttpPut]
        [Authorize]
        public IActionResult UpdateUser([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    User result = db.User.SingleOrDefault(buffer => buffer.Id == Utilities.GetUserId(HttpContext) && buffer.Password == Utilities.Sha256(data["CurrentPassword"].ToString()));
                    if (result == null)
                    {
                        return Unauthorized();
                    }
                    else
                    {
                        JObject entry = JObject.Parse(data["User"].ToString());
                        User body = entry.ToObject<User>();
                        IList<String> keys = entry.Properties().Select(p => p.Name).ToList();
                        foreach (String key in keys)
                        {
                            PropertyInfo pinfo = typeof(User).GetProperty(key);
                            object value = pinfo.GetValue(body, null);
                            pinfo.SetValue(result, value);
                        }
                        db.SaveChanges();
                        return Ok();
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}