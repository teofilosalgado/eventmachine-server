﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using WebApp.Attributes;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/UserEvent")]
    public class UserEventController : Controller
    {
        [Authorize]
        [HttpGet]
        [Route("GetUsersRole/{eventId:int}")]
        [MinimumUserRole(Utilities.Roles.Observer)]
        public IActionResult GetUsersRole(int eventId)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Userevent> data = db.Userevent.Where(buffer => buffer.Idevent == eventId);
                    JArray jArray = new JArray();
                    foreach (Userevent ue in data)
                    {
                        JObject temp = JObject.FromObject(new
                        {
                            ue.Iduser,
                            ue.Userrole
                        });
                        jArray.Add(temp);
                    }
                    return Ok(Json(jArray).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("GetUserEvents")]
        public IActionResult GetUserEvents()
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Userevent> queryable = db.Userevent.Where(buffer => buffer.Iduser == Utilities.GetUserId(HttpContext));
                    List<int> userRoles = new List<int>();
                    foreach (Userevent ue in queryable)
                    {
                        userRoles.Add(ue.Userrole);
                    }

                    List<int> avaibleRoles = userRoles.Distinct().ToList();

                    JObject toSend = new JObject();
                    foreach (int role in avaibleRoles)
                    {
                        JArray jArray = new JArray();
                        IQueryable<Userevent> temp = queryable.Where(buffer => (buffer.Userrole == role));

                        foreach (Userevent result in temp)
                        {
                            JObject jObject = JObject.FromObject(new
                            {
                                result.Idevent,
                            });
                            jArray.Add(jObject);

                        }
                        toSend.Add(role.ToString(), jArray);
                    }
                    return Ok(Json(toSend).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [MinimumUserRole(Utilities.Roles.Administrator)]
        [HttpPut]
        public IActionResult UpdateUserRole([FromBody]JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Userevent userevent = db.Userevent.SingleOrDefault(buffer => (buffer.Iduser == (int)data["UserId"]) && (buffer.Idevent == (int)data["EventId"]));
                    if (userevent == null)
                    {
                        return BadRequest();
                    }
                    else
                    {
                        userevent.Userrole = (int)data["Role"];
                        db.SaveChanges();
                        return Ok();
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [Authorize]
        [MinimumUserRole(Utilities.Roles.Administrator)]
        [HttpPost]
        public IActionResult AddUsertoEvent([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {

                    User userExists = db.User.SingleOrDefault(buffer => (buffer.Id == (int)data["UserId"]));
                    Event eventExists = db.Event.SingleOrDefault(buffer => (buffer.Id == (int)data["EventId"]));
                    if ((userExists == null) || (eventExists == null))
                    {
                        return BadRequest();
                    }
                    else
                    {
                        Userevent alreadyExists = db.Userevent.SingleOrDefault(buffer => (buffer.Iduser == (int)data["UserId"]) && (buffer.Idevent == (int)data["EventId"]));
                        if (alreadyExists == null)
                        {
                            Userevent toAdd = new Userevent();
                            toAdd.Iduser = (int)data["UserId"];
                            toAdd.Idevent = (int)data["EventId"];
                            db.Userevent.Add(toAdd);
                            db.SaveChanges();
                            return Ok();
                        }
                        else
                        {
                            return Ok();
                        }
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        //SPECIAL BUILT-IN AUTHORIZATION
        [Authorize]
        [HttpDelete]
        public IActionResult DeleteUserFromEvent([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    if ((int)data["UserId"] == Utilities.GetUserId(HttpContext))
                    {
                        IQueryable<Userevent> qry = db.Userevent.Where(buffer => (buffer.Idevent == (int)data["EventId"]) && (buffer.Userrole == (int)Utilities.Roles.Administrator));
                        if (qry.Count() == 1)
                        {
                            return BadRequest();
                        }
                        else
                        {
                            Userevent toRemove = db.Userevent.SingleOrDefault(buffer => (buffer.Idevent == (int)data["EventId"]) && (buffer.Iduser == Utilities.GetUserId(HttpContext)));
                            db.Userevent.Remove(toRemove);
                            db.SaveChanges();
                            return Ok();
                        }
                    }
                    else
                    {
                        if (db.Userevent.SingleOrDefault(buffer => (buffer.Iduser == Utilities.GetUserId(HttpContext)) && (buffer.Idevent == (int)data["EventId"]) && (buffer.Userrole == (int)Utilities.Roles.Administrator)) == null)
                        {
                            return Unauthorized();
                        }
                        else
                        {
                            Userevent toRemove = db.Userevent.SingleOrDefault(buffer => (buffer.Iduser == (int)data["UserId"]) && (buffer.Idevent == (int)data["EventId"]));
                            db.Remove(toRemove);
                            db.SaveChanges();
                            return Ok();
                        }
                    }
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}