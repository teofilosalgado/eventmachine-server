﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApp.Attributes;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/TicketBatch")]
    public class TicketBatchController : Controller
    {
        [HttpPost]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult CreateTicketBatch([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    Ticketbatch toSend = new Ticketbatch();
                    toSend = (data["TicketBatch"]).ToObject<Ticketbatch>();
                    db.Ticketbatch.Add(toSend);
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPut]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Contributor)]
        public IActionResult UpdateTicketBatch([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    int tbId = (int)data["TicketBatchId"];
                    Ticketbatch result = db.Ticketbatch.SingleOrDefault(buffer => (buffer.Id == tbId));

                    JObject entry = JObject.Parse(data["TicketBatch"].ToString());
                    Ticketbatch body = entry.ToObject<Ticketbatch>();
                    IList<String> keys = entry.Properties().Select(p => p.Name).ToList();
                    foreach (String key in keys)
                    {
                        PropertyInfo pinfo = typeof(Ticketbatch).GetProperty(key);
                        object value = pinfo.GetValue(body, null);
                        pinfo.SetValue(result, value);
                    }

                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        [Authorize]
        [MinimumUserRole(Utilities.Roles.Administrator)]
        public IActionResult DeleteTicketBatch([FromBody] JObject data)
        {
            try
            {
                using (DatabaseContext db = new DatabaseContext())
                {
                    int tbId = (int)data["TicketBatchId"];
                    IQueryable<Ticket> temp = db.Ticket.Where(buffer => (buffer.Idticketbatch == tbId));
                    foreach (Ticket tc in temp)
                    {
                        db.Ticket.Remove(tc);
                    }
                    db.Ticketbatch.Remove(db.Ticketbatch.SingleOrDefault(buffer => buffer.Id == tbId));
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize]
        [Route("GetEventTicketBatches/{eventId:int}")]
        [MinimumUserRole(Utilities.Roles.Observer)]
        public IActionResult GetEventTicketBatches(int eventId)
        {
            try
            {
                JArray jArray = new JArray();
                using (DatabaseContext db = new DatabaseContext())
                {
                    IQueryable<Ticketbatch> tb = db.Ticketbatch.Where(buffer => (buffer.Idevent == eventId));
                    foreach (Ticketbatch temp in tb)
                    {
                        JObject jObject = JObject.FromObject(temp);
                        jObject.Remove("Ticket");
                        jArray.Add(jObject);
                    }
                    return Ok(Json(jArray).Value);
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}